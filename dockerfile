FROM  php:7.3-fpm-alpine
MAINTAINER Yohan JEAN-THEODORE

#Install packages
RUN apk upgrade \
    apk add git zip \
    && curl -O https://getcomposer.org/composer.phar

#installation composer
RUN mv composer.phar /usr/local/bin/composer \
&& chmod +x /usr/local/bin/composer


#prepare application directory for Behat
RUN mkdir -p /opt/application/lib

ADD ./config_files/composer.json /opt/application/lib/

# Récupération librairies Behat
RUN cd /opt/application/lib && composer install \
&& ln -s /opt/application/lib/vendor/behat/behat/bin/behat /usr/local/bin/behat


ADD ./config_files/behat-entrypoint.sh    /usr/local/bin/behat-entrypoint
RUN chmod +x /usr/local/bin/behat-entrypoint

VOLUME /opt/application/results
VOLUME /opt/application/src
WORKDIR /opt/application/
ENTRYPOINT /usr/local/bin/behat-entrypoint
